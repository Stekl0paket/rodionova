import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Суперавтотостер', description: '', gender: 'female' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('Api поиска котика', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Поиск котика по id', async () => {
    const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      cat: {
        id: catId,
        ...cats[0],
        tags: null,
        likes: 0,
        dislikes: 0,
      }
    });
  });

  it('Поиск котика с некорректным id вызовет ошибку', async () => {
    await expect(
      HttpClient.get(`core/cats/get-by-id?id=${fakeId}`)
    ).rejects.toThrowError('Response code 400 (Bad Request)');
  });

});
